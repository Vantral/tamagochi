import copy
import random
import time
import telebot
from threading import Thread

# список тамагочи
tamagotchies = {}
dialog_with_player_status = {}

bot = telebot.TeleBot("6287737585:AAEJPp_haFsEGYXenp4ILZKjNKvWI4A83uM")


@bot.message_handler(commands=["start"])
def on_start(message):
    # если игрок ранее играл
    if message.from_user.id in tamagotchies:
        # если предыдущий тамагочи живой, умерщвляем его
        if tamagotchies[message.from_user.id].alive:
            tamagotchies[message.from_user.id].kill()
        # удаляем данные о предыдущей игре
        tamagotchies.pop(message.from_user.id)
    else:
        bot.send_message(message.from_user.id, "Привет!\nПоиграй в тамагочи\n")
    bot.send_message(message.from_user.id, "Как зовут твоего тамагочи?")

    # инициализируем диалог с игроком, чтобы получить имя и тип тамагочи
    dialog_with_player_status[message.from_user.id] = {"step": "wait_input_name", "name": " ",
                                                       "species": " "}


@bot.message_handler(commands=["status"])
def get_status(message):
    if (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        tamagotchies[message.from_user.id].send_tamagotchistate()
    else:
        bot.send_message(message.from_user.id, 'Для начала игры введите команду\n/start')
        if message.from_user.id in dialog_with_player_status:
            dialog_with_player_status.pop(message.from_user.id)


@bot.message_handler(commands=["kill"])
def get_status(message):
    if (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        tamagotchies[message.from_user.id].kill()
    else:
        bot.send_message(message.from_user.id, 'Для начала игры введите команду\n/start')
        if message.from_user.id in dialog_with_player_status:
            dialog_with_player_status.pop(message.from_user.id)


@bot.message_handler(content_types=["text"])
def on_command(message):
    if message.from_user.id in dialog_with_player_status:
        # если ведём диалог с игроком по вводу имени и типа тамагочи
        if dialog_with_player_status[message.from_user.id]["step"] == "wait_input_name":
            dialog_with_player_status[message.from_user.id]["name"] = message.text
            dialog_with_player_status[message.from_user.id]["step"] = "wait_input_species"
            bot.send_message(message.from_user.id, "Какого типа твой тамагочи?")
        elif dialog_with_player_status[message.from_user.id]["step"] == "wait_input_species":
            dialog_with_player_status[message.from_user.id]["species"] = message.text
            # здесь уже известно имя и тип тамагочи, поэтому создаём тамагочи и удаляем диалог с пользователем
            tamagotchies[message.from_user.id] = Tamagotchi(message.from_user.id, message.from_user.full_name,
                                                            dialog_with_player_status[message.from_user.id]["name"],
                                                            dialog_with_player_status[message.from_user.id]["species"])
            dialog_with_player_status.pop(message.from_user.id)
        else:
            bot.send_message(message.from_user.id, "Что-то пошло не так. Для начала игры введите команду\n/start")
    elif (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        # если играем с тамагочи
        command = {
            "1": "health",
            "2": "satiety",
            "3": "mood"
            # если введённый код не опознан, тогда команда - пустая строка
        }.get(message.text, "")

        if command and (message.from_user.id in tamagotchies):
            tamagotchies[message.from_user.id].increase_balance(command)
    else:
        bot.send_message(message.from_user.id, 'Для начала игры введите команду\n/start')


def send_message_to_player(owner_id, message):
    bot.send_message(owner_id, message)


# из основных методов класса: оповещение о состоянии тамагочи,
# как от самого тамагочи, так и от ведущего; изменение жизненных сил/ресурсов тамагочи
class Tamagotchi:
    # описание характеристик всех типов тамагочи (кот, пёс, черепаха и хомяк)
    all_species_tamagotchi = {
        # параметры тамагочи кота (самые обычные характеристики), наверное, потом пофиксить характеристики и некоторые
        # удалить
        "Кот": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Крепыш"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Улюлю"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_request": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я скорой уйду."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 0,
                        "status": "Ребенок"},
                "health": {"balance": 100, "decrease_step": -5, "interval": 60, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -7, "interval": 30, "increase_step": 10,
                            "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -1, "interval": 15, "increase_step": 10, "status": "Весёлый"}
            }
        },
        "Пёс": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Аляля"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_request": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я ухожу."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 0,
                        "status": "Ребенок"},
                "health": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -5, "interval": 30, "increase_step": 10,
                            "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -10, "interval": 15, "increase_step": 10, "status": "Весёлый"}
            }
        },
        "Черепаха": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Крепыш"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_request": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я ухожу."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 0,
                        "status": "Ребенок"},
                "health": {"balance": 100, "decrease_step": -3, "interval": 60, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -3, "interval": 30, "increase_step": 10,
                            "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -2, "interval": 15, "increase_step": 10, "status": "Весёлый"}
            }
        },
        #  у этого быстрее уходят разные характеристики (здоровье, голод)
        "Хомяк": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Здоровяк"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_request": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я ухожу."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -5, "interval": 60, "increase_step": 0,
                        "status": "Бэйби"},
                "health": {"balance": 100, "decrease_step": -8, "interval": 12, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -8, "interval": 6, "increase_step": 10, "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -6, "interval": 3, "increase_step": 10, "status": "Весёлый"}
            }
        }
    }

    # конструктор тамагочи
    def __init__(self, owner_id: str, owner_fullname: str, name: str, species: str):
        self.owner_id = owner_id
        self.owner_fullname = owner_fullname  # пока что нигде не используется
        self.name = name
        self.species = species

        # проверяем наличие указанного в параметре вида тамагочи в списке известных тамагочи,
        # и если совпадение не найдено, тогда считаем что свойства тамагочи соответствуют стандартному тамагочи
        # уже ненужная вещь, так как будут кнопки с выбором типа или что-то вроде
        search_species = species if species in self.all_species_tamagotchi else 'кот'

        # копируем все свойства тамагочи из данных соответствующего вида тамагочи

        # шкалы состояний для уведомлений
        self.scales_for_notify = copy.deepcopy(self.all_species_tamagotchi[search_species]["scales_for_notify"])
        # шкалы состояний для запросов тамагочи
        self.scales_for_request = copy.deepcopy(self.all_species_tamagotchi[search_species]["scales_for_request"])
        # параметры состояния тамагочи
        self.properties = copy.deepcopy(self.all_species_tamagotchi[search_species]["properties"])

        self.alive = True

        # это статус изменения состояния тамагочи
        # при изменении состояния определяем будет ли тамагочи просить о чём-то игрока
        # в начале считаем, что статус изменённый
        self.state_has_changed = True
        # определяем временные параметры сообщений тамагочи игроку, которые
        # тамагочи будет давать при изменении своего состояния, или через интервал времени при неизменном состоянии
        self.lasttime_request_action = 0
        self.interval_request_action = 30

        # определяем временные параметры уведомления игрока о состоянии тамагочи
        self.interval_notification_about_tamagotchi_state = 120

        # определяем временные параметры уведомления игрока о необходимости выполнения действия
        self.lasttime_notification_about_action = 0
        self.interval_notification_about_action = 50

        send_message_to_player(self.owner_id, f'Тамагочи {self} родился')

        # запускаем потоки уведомлений
        for func in [self.request_action, self.notify_about_tamagotchi_state, self.notify_about_action]:
            thread = Thread(target=func)
            # выставляем свойство daemon для завершения потока при окончании выполнения основной программы
            # (типа фоновые потоки)
            thread.daemon = True
            thread.start()

        # запускаем потоки жизненных процессов: здоровье, сытость, настроение
        for feature in self.properties:
            thread = Thread(target=self.decrease_balance, args=(feature,))
            thread.daemon = True
            thread.start()

    # метод, уменьшающий со временем значение ресурсов
    def decrease_balance(self, feature):
        while self.alive:
            time.sleep(self.properties[feature]["interval"])
            # сохраняем статус свойства для последующего контроля его изменения
            previous_status = self.properties[feature]["status"]
            # уменьшаем баланс состояния на случайную величину в пределах диапазона 0..лимит
            self.properties[feature]["balance"] += round(self.properties[feature]["decrease_step"] * random.random(),
                                                         2)
            # вычисляем текущий статус свойства
            self.properties[feature]["status"] = self.get_notify_scale_item_by_balance(feature)
            # сравниваем предыдущее и текущее значения статуса свойства, и если статус изменился,
            # тогда взводим переменную-флаг изменения состояния
            if previous_status != self.properties[feature]["status"]:
                self.state_has_changed = True
            # проверяем баланс свойства, и если значение меньше нижнего предела этого свойства,
            # тогда - увы, тамагочи умер
            if self.properties[feature]["balance"] < self.scales_for_notify[feature][-1][0]:
                self.kill()

    # метод, увеличивающий остаток ресурса по команде игрока
    def increase_balance(self, feature):
        # сохраняем статус свойства для последующего контроля его изменения
        previous_status = self.properties[feature]["status"]
        # увеличиваем баланс состояния на величину лимита
        self.properties[feature]["balance"] += self.properties[feature]["increase_step"]
        # вычисляем текущий статус свойства
        self.properties[feature]["status"] = self.get_notify_scale_item_by_balance(feature)
        # сравниваем предыдущее и текущее значения статуса свойства,
        # и если статус изменился, тогда взводим переменную-флаг изменения состояния
        if previous_status != self.properties[feature]["status"]:
            self.state_has_changed = True
        # обновляем время активности игрока
        self.lasttime_notification_about_action = time.time()

    def kill(self):
        if self.alive:
            send_message_to_player(self.owner_id, f'Тамагочи {self} умер')
        self.alive = False

    # метод, регулярно с периодичностью interval_notification_about_tamagotchi_state
    # оповещающий игрока о состоянии тамагочи
    def notify_about_tamagotchi_state(self):
        while self.alive:
            time.sleep(self.interval_notification_about_tamagotchi_state)
            # уведомление выдаём регулярно
            if self.alive:
                send_message_to_player(self.owner_id, f'Уведомление о {self.name}({self.species}): ' + ', '.join(
                    [self.properties[feature]["status"] for feature in self.properties]))

    # метод, реализующий запросы тамагочи на действие игрока.
    # если действие не требуется, тамагочи сообщает, что у него нет потребностей.
    # событие срабатывает при изменении состояния тамагочи, или через interval_request_action,
    # если состояние не изменилось за это время
    def request_action(self):
        while self.alive:
            current_time = time.time()
            # тамагочи просит о чём-то, если его статус изменился в худшую сторону, или напоминает о просьбе
            # (или своём состоянии) по прошествии времени interval_request_action
            if self.state_has_changed or ((self.lasttime_request_action + self.interval_request_action) < current_time):
                messages_list = list(filter(lambda x: x is not None,
                                            [self.get_request_scale_item_by_value(feature) for feature in
                                             self.properties]))
                send_message_to_player(self.owner_id, f'Сообщение от тамагочи {self.name}({self.species}):\n' + (
                    '\n'.join(messages_list) if len(messages_list) > 0 else 'У меня всё хорошо. Спасибо!'))
                self.state_has_changed = False
                self.lasttime_request_action = current_time
            time.sleep(1)

    # метод, периодически уведомляющий игрока о возможных действиях.
    # уведомление выдаётся при простое игрока дольше interval_notification_about_action
    def notify_about_action(self):
        while self.alive:
            current_time = time.time()
            # уведомление выдаём при простое игрока дольще чем interval_notification_about_action и в самом начале игры
            if (self.lasttime_notification_about_action + self.interval_notification_about_action) < current_time:
                send_message_to_player(self.owner_id,
                                       f'Введите команду:\n1 - полечить,\n2 - покормить,'
                                       f'\n3 - повеселить,\n/status - запрос состояния тамагочи')
                self.lasttime_notification_about_action = current_time
            time.sleep(self.interval_notification_about_action)

    # метод, возвращаюший состояние тамагочи
    def send_tamagotchi_state(self):
        send_message_to_player(self.owner_id, f'Состояние {self.name}( {self.species}): ' + ', '.join(
            [self.properties[feature]["status"] for feature in self.properties]))

    # метод возвращает описание состояния тамагочи по текущему значению свойства
    def get_notify_scale_item_by_balance(self, feature):
        for x in self.scales_for_notify[feature]:
            if self.properties[feature]["balance"] >= x[0]:
                return x[1]
        return "Никакой"

    # метод возвращает потребность тамагочи по указанному свойству
    def get_request_scale_item_by_value(self, feature):
        for x in self.scales_for_request[feature]:
            if self.properties[feature]["balance"] >= x[0]:
                return x[1]
        return None

    def __str__(self) -> str:
        return f'{self.name}({self.species})'

    def get_feature_balance(self, feature):
        return self.properties[feature]["balance"]


if __name__ == '__main__':
    bot.infinity_polling()
