from threading import Thread
from threading import Timer
import time
import random


class Tamagochi:
    # описание характеристик всех типов тамагочи
    all_species_tamagochi = {
        "кот": {
            "scales_for_notify": {
                "health": [
                    [120, "Здоровяк"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_request": {
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я ухожу."]
                ]
            },
            "properties": {
                "health": {"balance": 100, "decrease_step": -5, "interval": 60, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -3, "interval": 30, "increase_step": 10,
                            "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -2, "interval": 15, "increase_step": 10, "status": "Весёлый"}
            }
        },
        "черепаха": {
            "scales_for_notify": {
                "health": [
                    [120, "Здоровяк"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_request": {
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я ухожу."]
                ]
            },
            "properties": {
                "health": {"balance": 100, "decrease_step": -8, "interval": 12, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -5, "interval": 6, "increase_step": 10, "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -4, "interval": 3, "increase_step": 10, "status": "Весёлый"}
            }
        }
    }

    # конструктор тамагочи
    def __init__(self, name: str, species: str):
        self.name = name
        self.species = species

        search_species = species if species in self.all_species_tamagochi else 'кот'

        # копируем все свойства тамагочи из данных соответствующего вида тамагочи
        # шкалы состояний для уведомлений
        self.scales_for_notify = self.all_species_tamagochi[search_species]["scales_for_notify"].copy()
        # шкалы состояний для запросов тамагочи (None - ничего не нужно)
        self.scales_for_request = self.all_species_tamagochi[search_species]["scales_for_request"].copy()
        # параметры состояния тамагочи
        self.properties = self.all_species_tamagochi[search_species]["properties"].copy()

        self.alive = True

        # это статус изменения состояния тамагочи
        # при изменении состояния определяем будет ли тамагочи просить о чём-то игрока
        # в начале считаем, что статус изменённый
        self.state_has_changed = True
        # определяем временные параметры сообщений тамагочи игроку,
        # которые тамагочи будет давать при изменении своего состояния,
        # или через интервал времени при неизменном состоянии
        self.lasttime_request_action = 0
        self.interval_request_action = 15

        # определяем временные параметры уведомления игрока о состоянии тамагочи
        self.interval_notification_about_tamagochistate = 20

        # определяем временные параметры уведомления игрока о необходимости выполнения действия
        self.lasttime_notification_about_action = 0
        self.interval_notification_about_action = 30

        # запускаем потоки уведомлений
        Thread(target=self.notify_about_action).start()
        Thread(target=self.request_action).start()
        Thread(target=self.notify_about_tamagochi_state).start()

        # запускаем потоки жизненных процессов: здоровье, сытость, настроение
        for feature in self.properties:
            Thread(target=self.decrease_balance, args=(feature,)).start()

    # метод, уменьшающий со временем остаток ресурса
    def decrease_balance(self, feature):
        while self.alive:
            time.sleep(self.properties[feature]["interval"])
            # сохраняем статус свойства для последующего контроля его изменения
            previous_status = self.properties[feature]["status"]
            # уменьшаем баланс состояния на случайную величину в пределах диапазона 0..лимит
            self.properties[feature]["balance"] += round(self.properties[feature]["decrease_step"] * random.random(), 2)
            # вычисляем текущий статус свойства
            self.properties[feature]["status"] = self.get_notify_scale_item_by_balance(feature)
            # сравниваем предыдущее и текущее значения статуса свойства,
            # и если статус изменился, тогда взводим переменную-флаг изменения состояния
            if previous_status != self.properties[feature]["status"]:
                self.state_has_changed = True
            # проверяем баланс свойства, и если значение ниже предела,
            # тогда - тамагочи мертв
            if self.properties[feature]["balance"] < self.scales_for_notify[feature][-1][0]:
                self.kill()

    # метод, увеличивающий остаток ресурса по команде игрока
    def increase_balance(self, feature):
        # сохраняем статус свойства для последующего контроля его изменения
        previous_status = self.properties[feature]["status"]
        # увеличиваем баланс состояния на величину лимита
        self.properties[feature]["balance"] += self.properties[feature]["increase_step"]
        # вычисляем текущий статус свойства
        self.properties[feature]["status"] = self.get_notify_scale_item_by_balance(feature)
        # сравниваем предыдущее и текущее значения статуса свойства,
        # и если статус изменился, тогда взводим переменную-флаг изменения состояния
        if previous_status != self.properties[feature]["status"]:
            self.state_has_changed = True
        # так как лимит увеличился по команде игрока, устанавливаем время крайней реакции игрока в текущее время
        self.lasttime_notification_about_action = time.time()

    def kill(self):
        self.alive = False

    # метод, периодически уведомляющий игрока о возможных действиях.
    # уведомление выдаётся при простое игрока дольше interval_notification_about_action
    def notify_about_action(self):
        while self.alive:
            current_time = time.time()
            # уведомление выдаём при простое игрока дольще чем interval_notification_about_action и в самом начале игры
            if (self.lasttime_notification_about_action + self.interval_notification_about_action) < current_time:
                print(f'Введите команду: 1 - полечить, 2 - покормить, 3 - повеселить')
                self.lasttime_notification_about_action = current_time
            time.sleep(self.interval_notification_about_action)

    # метод, регулярно оповещающий игрока о состоянии тамагочи
    def notify_about_tamagochi_state(self):
        while self.alive:
            time.sleep(self.interval_notification_about_tamagochistate)
            # уведомление выдаём регулярно
            if self.alive:
                print(f'Уведомление о {self.name}({self.species}): ' + ', '.join(
                    [self.properties[feature]["status"] for feature in self.properties]))

    # метод, реализующий запросы тамагочи на действие игрока.
    # если действие не требуется, тамагочи сообщает, что у него нет потребностей.
    def request_action(self):
        while self.alive:
            current_time = time.time()
            # тамагочи просит о чём-то, если его статус изменился в худшую сторону,
            # или напоминает о просьбе(или своём состоянии) по прошествии времени interval_request_action
            if self.state_has_changed or ((self.lasttime_request_action + self.interval_request_action) < current_time):
                messages_list = list(filter(lambda x: x is not None,
                                            [self.get_request_scale_item_by_value(feature) for feature in
                                             self.properties]))
                print(f'Сообщение от тамагочи {self.name}({self.species}): ' + (
                    ', '.join(messages_list) if len(messages_list) > 0 else 'У меня всё хорошо. Спасибо!'))
                self.state_has_changed = False
                self.lasttime_request_action = current_time
            time.sleep(1)

    # метод возвращает описание состояния тамагочи по текущему значению свойства
    def get_notify_scale_item_by_balance(self, feature):
        for x in self.scales_for_notify[feature]:
            if self.properties[feature]["balance"] >= x[0]:
                return x[1]
        return "Никакой"

    # метод возвращает потребность тамагочи по указанному свойству
    def get_request_scale_item_by_value(self, feature):
        for x in self.scales_for_request[feature]:
            if self.properties[feature]["balance"] >= x[0]:
                return x[1]
        return None

    def __str__(self) -> str:
        return f'{self.name}({self.species})'

    def get_feature_balance(self, feature):
        return self.properties[feature]["balance"]


# сосоздаём нашего тамагочи
tamagochi = Tamagochi("Тэд", "кот")

try:
    while tamagochi.alive:
        # каждый раз создаём таймер для ожидания ввода игроком команды
        t = Timer(10, lambda: None)
        t.start()

        command_code = input()
        command = {
            "1": "health",
            "2": "satiety",
            "3": "mood"
            # если введённый код не опознан, тогда команда - пустая строка
        }.get(command_code, "")

        if command:
            tamagochi.increase_balance(command)

        t.cancel()

        if not tamagochi.alive:
            raise RuntimeError

except RuntimeError:
    print('Тамагочи умер')
