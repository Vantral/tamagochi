import copy
import random
import time
import telebot
from threading import Thread
import ast
import re

tamagotchies = {}

#  словарь с актуальными игроками
players_dict = {}

# список ведущихся диалога с игроками
dialog_with_player_status = {}

shopping_list = {}

bot = telebot.TeleBot("6287737585:AAEJPp_haFsEGYXenp4ILZKjNKvWI4A83uM")

other_species_tamagotchi = {}
# читаем описание типов тамагочи
with open("./other_species.json", "r", encoding="utf-8") as f:
    other_species_tamagotchi = ast.literal_eval(f.read())  # эта конструкция ast преобразовывает строку в словарь

species_tamagotchi = copy.deepcopy(other_species_tamagotchi)

intervals = (
    ('Дни', 86400),  # 60 * 60 * 24
    ('Часы', 3600),  # 60 * 60
    ('Минуты', 60),
    ('Секунды', 1)
)


# обработчик старта игры
@bot.message_handler(commands=["start"])
def on_start(message):
    # если игрок ранее играл
    if message.from_user.id in tamagotchies:
        # если предыдущий тамагочи живой, умерщвляем его
        if tamagotchies[message.from_user.id].alive:
            tamagotchies[message.from_user.id].kill()
        # удаляем данные о предыдущей игре
        tamagotchies.pop(message.from_user.id)
    else:
        bot.send_message(message.from_user.id, "Привет!\nПоиграй в тамагочи\n")
    bot.send_message(message.from_user.id, "Как зовут твоего тамагочи?")

    # инициализируем диалог с игроком, чтобы получить имя и тип тамагочи
    dialog_with_player_status[message.from_user.id] = {"step": "wait_input_name", "name": " ",
                                                       "species": " "}


# обработчик запроса состояния тамагочи
@bot.message_handler(commands=["status"])
def get_status(message):
    if (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        tamagotchies[message.from_user.id].send_tamagotchi_state()
    else:
        bot.send_message(message.from_user.id, 'Для начала игры введите команду\n/start')
        if message.from_user.id in dialog_with_player_status:
            dialog_with_player_status.pop(message.from_user.id)


# обработчик запроса умертвить тамагочи
@bot.message_handler(commands=["kill"])
def get_status(message):
    if (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        tamagotchies[message.from_user.id].kill()
    else:
        bot.send_message(message.from_user.id, 'Для начала игры введите команду\n/start')
        if message.from_user.id in dialog_with_player_status:
            dialog_with_player_status.pop(message.from_user.id)


# Обработчик запроса купить тамагочи что-то из магазина.
@bot.message_handler(commands=["store"])
def get_status(message):
    if (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        bot.send_message(message.from_user.id, 'Выберите чем хотите побаловать Тамагочи:\n'
                                               'sweets\n'
                                               'food\n'
                                               'relax')


# Обработчик запроса узнать статус (кол-во монет).
@bot.message_handler(commands=["balance"])
def get_status(message):
    if (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        bot.send_message(message.from_user.id,
                         'Ваш баланс: ' + str(tamagotchies[message.from_user.id].coins) + ' монет.')


# Вытаскиваем действия.
def get_available(name):
    list_of_a = ["Ничего"]
    if name == "sweets":
        list_of_a = [i for i in Store.sweets]
    elif name == "relax":
        list_of_a = [i for i in Store.relax]
    elif name == "food":
        list_of_a = [i for i in Store.food]

    return list_of_a


# Метод вытаскивает характеристики действия (описание, стоимость) или говорит, что такого действия нет.
def check_activ(name):
    if name in Store.sweets:
        return Store.sweets[name]['description'] + "\n" + "Стоимость " + str(Store.sweets[name]['cost'])
    elif name in Store.relax:
        return Store.relax[name]['description'] + "\n" + "Стоимость " + str(Store.relax[name]['cost'])
    elif name in Store.food:
        return Store.food[name]['description'] + "\n" + "Стоимость " + str(Store.food[name]['cost'])
    return 'Такого в магазине нет.'


# обработчик запроса списка тамагочи
@bot.message_handler(commands=["list"])
def get_list(message):
    # формируем текст со списком
    if len(players_dict) > 0:
        bot.send_message(message.from_user.id, '\n'.join([str(tamagotchies[players_dict[tamagotchi_name]]) + ': ' +
                                                          tamagotchies[players_dict[tamagotchi_name]].owner_fullname for
                                                          tamagotchi_name in players_dict]))
    else:
        bot.send_message(message.from_user.id, "Игроков нет")

    # если с игроком вёлся диалог, сбрасываем его
    if message.from_user.id in dialog_with_player_status:
        dialog_with_player_status.pop(message.from_user.id)


# обработчик команд и сообщений игроков
@bot.message_handler(content_types=["text"])
def message_handler(message):
    # Убираем возможность выбора типа животного после того, как он был совершен.
    if message.text in ('Кот', 'Собака', 'Черепаха', 'Хомяк'):
        a = telebot.types.ReplyKeyboardRemove()
        bot.send_message(message.from_user.id, 'Тип тамагочи выбран.', reply_markup=a)
    # Убираем возможность выбора купить что-то из магазина, как он был совершен.
    elif message.text in ('Да', 'Нет'):
        a = telebot.types.ReplyKeyboardRemove()
        # Если пользователь согласился купить что-то из магазина и у него достаточный баланс:
        if message.text == 'Да' and tamagotchies[message.from_user.id].coins \
                - get_cost(shopping_list[message.from_user.id][0], shopping_list[message.from_user.id][1]) >= 0:
            bot.send_message(message.from_user.id, 'Покупка совершена. С вашего счета списаны монеты', reply_markup=a)
            # Вычитаем из баланса сумму покупки
            tamagotchies[message.from_user.id].coins -= \
                get_cost(shopping_list[message.from_user.id][0], shopping_list[message.from_user.id][1])
            # Повышаем характеристики тамагочи на указанное количество единиц.
            x, y = get_balance_store(shopping_list[message.from_user.id][0],
                                     shopping_list[message.from_user.id][1])
            tamagotchies[message.from_user.id].increase_balance_store(x, y)
        elif message.text == 'Да' and tamagotchies[message.from_user.id].coins \
                - get_cost(shopping_list[message.from_user.id][0], shopping_list[message.from_user.id][1]) < 0:
            bot.send_message(message.from_user.id, 'Покупка не совершена. На вашем счете недостаточно средств.',
                             reply_markup=a)
        else:
            bot.send_message(message.from_user.id, 'Покупка не совершена. С вашего счета не списаны монеты.',
                             reply_markup=a)
        del shopping_list[message.from_user.id]
        # Работает с разделами действий, покупаемыми в магазине.
    elif message.text.lower() in ('relax', 'sweets', 'food'):
        shopping_list[message.from_user.id] = [message.textlower()]
        bot.send_message(message.from_user.id, '\n'.join(get_available(message.textlower())))
    # Работает с действиями, покупаемыми в магазине.
    elif message.text.capitalize() in Store.list_of_activ:
        shopping_list[message.from_user.id].append(message.text.capitalize())
        bot.send_message(message.from_user.id, check_activ(message.text.capitalize()))
        bot.send_message(message.from_user.id, 'Покупаете?', reply_markup=get_agree())
    if message.from_user.id in dialog_with_player_status:
        # если ведём диалог с игроком по вводу имени и типа тамагочи
        if dialog_with_player_status[message.from_user.id]["step"] == "wait_input_name":
            # проверяем используется ли сейчас введённое имя, и если используется, просим придумать другое имя
            if message.text in players_dict:
                bot.send_message(message.from_user.id,
                                 "Это имя сейчас занято. Попробуй подобрать другое.\nКак назовёшь тамагочи?")
            else:
                # введенное имя не используется - сохраняем имя в диалоге и переходим к запросу типа тамагочи
                dialog_with_player_status[message.from_user.id]["name"] = message.text
                dialog_with_player_status[message.from_user.id]["step"] = "wait_input_species"
                bot.send_message(message.from_user.id, "Какого типа твой тамагочи?", reply_markup=get_types())
        elif dialog_with_player_status[message.from_user.id]["step"] == "wait_input_species":
            dialog_with_player_status[message.from_user.id]["species"] = message.text
            # здесь уже известно имя и тип тамагочи, поэтому создаём тамагочи и удаляем диалог с пользователем
            tamagotchies[message.from_user.id] = Tamagotchi(message.from_user.id, message.from_user.full_name,
                                                            dialog_with_player_status[message.from_user.id]["name"],
                                                            dialog_with_player_status[message.from_user.id]["species"])
            dialog_with_player_status.pop(message.from_user.id)
        else:
            bot.send_message(message.from_user.id, "Что-то пошло не так. Для начала игры введите команду\n/start")
    elif (message.from_user.id in tamagotchies) and tamagotchies[message.from_user.id].alive:
        # если играем с тамагочи
        command = {
            "1": "health",
            "2": "satiety",
            "3": "mood"
            # если введённый код не опознан, тогда команда - пустая строка
        }.get(message.text, "")

        # если игрок управляет тамагочи
        if command:
            #  либо действие на повышение потребностей у питомца, либо это уже сообщение! от пользователя (далее)
            if message.from_user.id in tamagotchies:
                tamagotchies[message.from_user.id].increase_balance(command)
                # С каждым действием добавляется количество монеток (на 5).
                tamagotchies[message.from_user.id].coins += 5
        else:
            # проверяем, обращение ли к другому тамагочи введено игроком
            message_parts = re.split('\s*,\s*', message.text.strip(), 1)
            # проверяем состоит текст из обращения с текстом, или нет
            if len([part for part in message_parts if part]) == 2:
                # если есть такой тамагочи, и это не тамагочи самого игрока!:)
                if message_parts[0] in players_dict:
                    if message.from_user.id == players_dict[message_parts[0]]:
                        bot.send_message(message.from_user.id, 'Ты разговариваешь сам с собой :)')
                    else:
                        # для стиля преобразуем первую букву текста к верхнему регистру (чтоб красивенько было)
                        bot.send_message(players_dict[message_parts[0]],
                                         "От '" + str(tamagotchies[message.from_user.id]) + "':\n" + message_parts[1][
                                             0].upper() + message_parts[1][1:])
                else:
                    bot.send_message(message.from_user.id, "Тамагочи с именем '" + message_parts[0] + "' в игре нет")
    else:
        bot.send_message(message.from_user.id, 'Для начала игры введите команду\n/start')


#  функция, отвечающая за отправку уведомлений пользователю
def send_message_to_player(owner_id, message):
    bot.send_message(owner_id, message)


# Создание кнопок для выбора типа тамагочи.
def get_types():
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    markup.add(telebot.types.KeyboardButton('Кот'), telebot.types.KeyboardButton('Собака'),
               telebot.types.KeyboardButton('Черепаха'), telebot.types.KeyboardButton('Хомяк'))
    return markup


# Создание кнопок да/нет для покупки.
def get_agree():
    markup = telebot.types.ReplyKeyboardMarkup(row_width=2, resize_keyboard=True)
    markup.add(telebot.types.KeyboardButton('Да'), telebot.types.KeyboardButton('Нет'))
    return markup


#  try и except используем для избежания ошибки (это конструкция),
#  то есть, например, у админа нет на компе папки с фотками, тогда, эта функция ничего не сделает (pass),
#  папка есть - фотки отправляются
def send_photo_to_player(owner_id, path):
    try:
        bot.send_photo(owner_id, photo=open(path, 'rb'))
    except Exception as err:
        pass


# вызов метода по имени для любого объекта
def call_method(obj, call_method_name):
    return getattr(obj, call_method_name)()


# из основных методов класса: оповещение о состоянии тамагочи,
# как от самого тамагочи, так и от ведущего; изменение жизненных сил/ресурсов тамагочи
class Tamagotchi:
    # описание характеристик всех типов тамагочи (обычный, кот, пёс, черепаха и хомяк)
    all_species_tamagotchi = species_tamagotchi

    # конструктор тамагочи
    def __init__(self, owner_id: str, owner_fullname: str, name: str, species: str, coins=0):
        self.owner_id = owner_id
        self.owner_fullname = owner_fullname  # пока что нигде не используется
        self.name = name
        self.species = species.capitalize()
        self.coins = coins

        # проверяем наличие указанного в параметре вида тамагочи в списке известных тамагочи,
        # и если совпадение не найдено, тогда считаем что свойства тамагочи соответствуют стандартному тамагочи
        # уже ненужная вещь, так как будут кнопки с выбором типа или что-то вроде, а может и не будет...
        search_species = self.species if self.species in self.all_species_tamagotchi else "Обычный"

        # копируем все свойства тамагочи из данных соответствующего вида тамагочи

        # шкалы состояний для уведомлений
        self.scales_for_notify = copy.deepcopy(self.all_species_tamagotchi[search_species]["scales_for_notify"])
        # шкалы состояний для запросов тамагочи
        self.scales_for_request = copy.deepcopy(self.all_species_tamagotchi[search_species]["scales_for_request"])
        # параметры состояния тамагочи
        self.features = copy.deepcopy(self.all_species_tamagotchi[search_species]["features"])
        # определяем метод, вызываемый посмертно
        self.death_report_method = self.all_species_tamagotchi[search_species][
            "method_called_after_death"] if "method_called_after_death" in self.all_species_tamagotchi[
            search_species] else None
        self.event_images = {}
        # если у типа есть картинки жизненных состояний, формируем из них словарик
        for image_key in ["birth_image", "happiest_image", "death_image"]:
            self.event_images[image_key] = self.all_species_tamagotchi[search_species][image_key] \
                if image_key in self.all_species_tamagotchi[search_species] else None

        self.alive = True
        # фиксируем время рождения
        self.birth_time = time.time()

        # это статус изменения состояния тамагочи
        # при изменении состояния определяем будет ли тамагочи просить о чём-то игрока
        # в начале считаем, что статус изменённый
        self.state_has_changed = True
        # определяем временные параметры сообщений тамагочи игроку, которые
        # тамагочи будет давать при изменении своего состояния, или через интервал времени при неизменном состоянии
        self.lasttime_request_action = 0
        self.interval_request_action = 30
        # определяем периодичность уведомления игрока о состоянии тамагочи
        self.interval_notification_about_tamagotchi_state = 120
        # определяем временные параметры уведомления игрока о необходимости выполнения действия
        self.lasttime_notification_about_action = 0
        self.interval_notification_about_action = 50
        # определяем периодичность поиска друзей
        self.interval_search_for_friend = 180

        if "birth_image" in self.event_images:
            send_photo_to_player(self.owner_id, self.event_images["birth_image"])
            send_message_to_player(self.owner_id, f'Тамагочи {self} родился')
        # записываем новорожденного в список тамагочи
        players_dict[self.name] = self.owner_id

        # запускаем потоки уведомлений
        for func in [self.request_action, self.notify_about_tamagotchi_state, self.notify_about_action,
                     self.search_for_friend]:
            thread = Thread(target=func)
            # выставляем свойство daemon для завершения потока при окончании выполнения основной программы
            # (типа фоновые потоки)
            thread.daemon = True
            thread.start()

        # запускаем потоки жизненных процессов: здоровье, сытость, настроение
        for feature in self.features:
            thread = Thread(target=self.decrease_balance, args=(feature,))
            thread.daemon = True
            thread.start()

    # метод, уменьшающий со временем значение ресурсов
    def decrease_balance(self, feature):
        while self.alive:
            time.sleep(self.features[feature]["interval"])
            self.change_balance(feature, round(
                self.features[feature]["decrease_step"] * self.get_feature_ratio(feature, "decrease") * random.random(),
                1))

    # метод, увеличивающий остаток ресурса по команде игрока
    def increase_balance(self, feature):
        # увеличиваем баланс состояния на величину лимита
        self.change_balance(feature,
                            self.features[feature]["increase_step"] * self.get_feature_ratio(feature, "increase"))
        # обновляем время активности игрока
        self.lasttime_notification_about_action = time.time()

    # Метод, увеличивающий остаток ресурса с помощью магазина.
    def increase_balance_store(self, feature, value):
        self.change_balance(feature, value)

    # наказание за перекорм питомца
    def decrease_health_for_overeating(self):
        # уменьшаем здоровье перекормленного питомца
        self.change_balance("health", round(
            self.features["health"]["decrease_step"] * self.get_feature_ratio("health", "decrease") * random.random(),
            1))

    # метод, меняющий значение характеристики тамагочи
    def change_balance(self, feature, value):
        # сохраняем статус свойства для последующего контроля его изменения
        previous_status = self.features[feature]["status"]
        # изменяем баланс состояния на value
        self.features[feature]["balance"] += value
        # вычисляем текущий статус свойства
        self.features[feature]["status"] = self.get_notify_scale_item_by_balance(feature)
        # проверяем баланс свойства, и если значение меньше нижнего предела этого свойства, тогда тамагочи умер
        if self.features[feature]["balance"] < self.scales_for_notify[feature][-1][0]:
            self.kill()
            return

        if previous_status != self.features[feature]["status"]:
            self.state_has_changed = True
        self.call_extra_functions(feature)

    def kill(self):
        if self.alive:
            self.alive = False
            # удаляем умершего из списка тамагочи, но на всякий случай с проверкой наличия в этом списке
            if self.name in players_dict:
                players_dict.pop(self.name)

            # отправляем посмертную картинку(если есть), сообщения
            if "death_image" in self.event_images:
                send_photo_to_player(self.owner_id, self.event_images["death_image"])
                send_message_to_player(self.owner_id, f'Тамагочи {self} умер')
            if self.death_report_method:
                call_method(self, self.death_report_method)

    def death_report(self):
        if self.features["age"]["balance"] < 0:
            send_message_to_player(self.owner_id, f'Причина смерти {self} - старость')
        elif self.features["satiety"]["status"] == "Объевшийся" and self.features["health"]["balance"] < 0:
            send_message_to_player(self.owner_id, f'Причина смерти {self} - переедание')

    # метод, регулярно с периодичностью interval_notification_about_tamagotchi_state
    # оповещающий игрока о состоянии тамагочи
    def notify_about_tamagotchi_state(self):
        while self.alive:
            time.sleep(self.interval_notification_about_tamagotchi_state)
            # уведомление выдаём регулярно
            if self.alive:
                self.send_suitable_image()
                send_message_to_player(self.owner_id, f'Уведомление о {self.name}({self.species}): ' + ', '.join(
                    [self.features[feature]["status"] for feature in self.features]))

    # Метод, реализующий запросы тамагочи на действие игрока.
    # Если действие не требуется, тамагочи сообщает, что у него нет потребностей.
    # Событие срабатывает при изменении состояния тамагочи, или через interval_request_action,
    # если состояние не изменилось за это время
    def request_action(self):
        while self.alive:
            current_time = time.time()
            # тамагочи просит о чём-то, если его статус изменился в худшую сторону, или напоминает о просьбе
            # (или своём состоянии) по прошествии времени interval_request_action
            if self.state_has_changed or ((self.lasttime_request_action + self.interval_request_action) < current_time):
                messages_list = list(filter(lambda x: x is not None,
                                            [self.get_request_scale_item_by_value(feature) for feature in
                                             self.features]))
                if len(messages_list) > 0:
                    self.send_suitable_image()
                else:
                    if "happiest_image" in self.event_images:
                        send_photo_to_player(self.owner_id, self.event_images["happiest_image"])
                        send_message_to_player(self.owner_id, f'Сообщение от тамагочи {self.name}({self.species}):\n' +
                                               ('\n'.join(messages_list) if len(messages_list) > 0
                                                else 'У меня всё хорошо. Спасибо!'))
                self.state_has_changed = False
                self.lasttime_request_action = current_time
            time.sleep(1)

    # Метод, периодически уведомляющий игрока о возможных действиях.
    def notify_about_action(self):
        while self.alive:
            current_time = time.time()
            # уведомление выдаём при простое игрока дольше чем interval_notification_about_action и в самом начале игры
            if (self.lasttime_notification_about_action + self.interval_notification_about_action) < current_time:
                send_message_to_player(self.owner_id,
                                       f'Введите команду:\n1 - полечить,\n2 - покормить,'
                                       f'\n3 - повеселить,\n/status - запрос состояния тамагочи'
                                       f'\n/store - вызвать магазин\n/balance - проверить баланс'
                                       f'\nКаждое действие увеличивает ваш баланс на 5 монеток!')
                self.lasttime_notification_about_action = current_time
            time.sleep(self.interval_notification_about_action)

    # метод, реализующий поиск друзей в игре
    def search_for_friend(self):
        while self.alive:
            # другого тамагочи ищем через равные интервалы времени
            time.sleep(self.interval_search_for_friend)
            # проверим, что наш тамагочи ещё жив
            if self.alive:
                # ищем случайного тамагочи в списке
                friend_name, friend_owner = random.choice(list(players_dict.items()))
                # проверим, что пользователь выбрал не самого себя
                if self.owner_id != friend_owner:
                    send_message_to_player(friend_owner, f'От {self}:\n{friend_name}, привет!')

    # метод, возвращающий состояние тамагочи
    def send_tamagotchi_state(self):
        self.send_suitable_image()
        send_message_to_player(self.owner_id, f'Состояние {self.name} ({self.species}): ' + ', '.join(
            [self.features[feature]["status"] for feature in self.features]))

    # метод возвращает описание состояния тамагочи по текущему значению свойства
    def get_notify_scale_item_by_balance(self, feature):
        for x in self.scales_for_notify[feature]:
            if self.features[feature]["balance"] >= x[0]:
                return x[1]
        return "Никакой"

    # метод возвращает потребность тамагочи по указанному свойству
    def get_request_scale_item_by_value(self, feature):
        for x in self.scales_for_request[feature]:
            if self.features[feature]["balance"] >= x[0]:
                return x[1]
        return None

    def get_feature_ratio(self, feature, direction: str = "decrease"):
        for x in self.scales_for_notify["age"]:
            if self.features["age"]["balance"] >= x[0]:
                if (len(x) < 3) or (not isinstance(x[2], dict)):
                    return 1
                return x[2].get('_'.join([feature, direction, "ratio"]), 1)
        return 1

    # метод, вызывающий extra_functions
    def call_extra_functions(self, feature):
        for x in self.scales_for_notify[feature]:
            if self.features[feature]["balance"] >= x[0]:
                if (len(x) < 3) or (not isinstance(x[2], dict)):
                    return
                for func in x[2].get("extra_functions", []):
                    call_method(self, func)
                return

    # метод выбирает картинку, соответствующую худшему из состояний
    def send_suitable_image(self):
        min_balance = float("inf")
        photo = None

        for feature in self.features:
            if self.features[feature]["balance"] < min_balance:
                for x in self.scales_for_notify[feature]:
                    if self.features[feature]["balance"] >= x[0]:
                        if (len(x) > 3) and isinstance(x[3], str):
                            photo = x[3]
                            min_balance = self.features[feature]["balance"]
                        break
        if photo:
            send_photo_to_player(self.owner_id, photo)

    def __str__(self) -> str:
        return f'{self.name}({self.species})'

    def get_feature_balance(self, feature):
        return self.features[feature]["balance"]


# Метод, узнающий цену покупки в магазине.
def get_cost(name, item):
    if name == 'sweets':
        return Store.sweets[item]['cost']
    elif name == 'food':
        return Store.food[item]['cost']
    elif name == 'relax':
        return Store.relax[item]['cost']


# Метод, узнающий кол-во единиц, на которое покупка в магазине повышает характеристики тамагочи.
def get_balance_store(name, item):
    if name == 'sweets':
        return 'mood', Store.sweets[item]['benefit']['mood']
    elif name == 'food':
        return 'satiety', Store.food[item]['benefit']['satiety']
    elif name == 'relax':
        return 'health', Store.relax[item]['benefit']['health']


# Действия, которые можно купить в магазине, с ценой и влиянием на характеристики.
class Store:
    list_of_activ = ["Конфетка", "Сладкая вата", "Большая коробка сладостей", "Напиток здоровья", "Массаж", "Санаторий",
                     "Фаст фуд", "Столовая", "Ресторан"]
    sweets = {"Конфетка":
                  {"description": "Конфетка дает + 5 к счастью вашего тамагочи",
                   "cost": 15,
                   "benefit": {"health": 0, "mood": 5, "satiety": 0}},
              "Сладкая вата":
                  {"description": "Сладкая вата дает + 15 к cчастью вашего тамагочи",
                   "cost": 50,
                   "benefit": {"health": 0, "mood": 15, "satiety": 0}},
              "Большая коробка сладостей":
                  {"description": 'Большая коробка сладостей дает + 30 к счастью вашего тамагочи',
                   "cost": 100,
                   "benefit": {"health": 0, "mood": 30, "satiety": 0}}}

    relax = {"Напиток здоровья": {"description": "Напиток здоровья дает + 5 к здоровью вашего тамагочи",
                                  "cost": 15,
                                  "benefit": {"health": 5, "mood": 0, "satiety": 0}},
             "Массаж": {'description': "Массаж дает + 15 к здоровью вашего тамагочи",
                        "cost": 50,
                        "benefit": {"health": 15, "mood": 0, "satiety": 0}},
             "Санаторий": {'description': " Санаторий дает + 30 к здоровью вашего тамагочи",
                           "cost": 100,
                           "benefit": {"health": 30, "mood": 0, "satiety": 0}}}
    food = {"Фаст фуд": {"description": "Фаст фуд дает + 5 к сытости вашего тамагочи",
                         "cost": 15,
                         "benefit": {"health": 0, "mood": 0, "satiety": 5}},
            "Столовая": {"description": "Столовая дает + 15 к сытости вашего тамагочи",
                         "cost": 50,
                         "benefit": {"health": 0, "mood": 0, "satiety": 15}},
            "Ресторан": {"description": "Ресторан дает + 30 к сытости вашего тамагочи",
                         "cost": 100,
                         "benefit": {"health": 0, "mood": 0, "satiety": 30}}}


if __name__ == '__main__':
    bot.infinity_polling()
