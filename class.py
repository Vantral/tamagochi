# из основных методов класса: оповещение о состоянии тамагочи,
# как от самого тамагочи, так и от ведущего; изменение жизненных сил/ресурсов тамагочи
class Tamagotchi:
    # описание характеристик всех типов тамагочи (кот, пёс, черепаха и хомяк)
    all_species_tamagotchi = {
        # параметры тамагочи кота (самые обычные характеристики), наверное, потом пофиксить характеристики и некоторые
        # изменить. У кота быстро падает голод (медленнее, чем у пса), настроение почти не падает
        "кот": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Крепыш"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_query": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я скорой уйду."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 0,
                        "status": "Ребенок"},
                "health": {"balance": 100, "decrease_step": -5, "interval": 60, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -7, "interval": 30, "increase_step": 10,
                            "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -1, "interval": 15, "increase_step": 10, "status": "Весёлый"}
            }
        },
        # у собаки будет быстро падать настроение, так как зависима от хозяина, и голод
        "Пёс": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Крепыш"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_query": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я уйду."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 0,
                        "status": "Ребенок"},
                "health": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -5, "interval": 30, "increase_step": 10,
                            "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -10, "interval": 15, "increase_step": 10, "status": "Весёлый"}
            }
        },
        "Черепаха": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Крепыш"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_query": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я уйду."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -1, "interval": 60, "increase_step": 0,
                        "status": "Ребенок"},
                "health": {"balance": 100, "decrease_step": -3, "interval": 60, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -3, "interval": 30, "increase_step": 10,
                            "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -2, "interval": 15, "increase_step": 10, "status": "Весёлый"}
            }
        },
        #  у этого быстрее уходят разные характеристики (здоровье, голод)
        "Хомяк": {
            "scales_for_notify": {
                "age": [
                    [100, "Ребенок"],
                    [87, "Подросток"],
                    [80, "Молодой"],
                    [60, "Взрослый"],
                    [20, "Старый"],
                    [0, "Прожил жизнь"]
                ],
                "health": [
                    [120, "Здоровяк"],
                    [75, "Здоровый"],
                    [60, "Недомогающий"],
                    [40, "Хворающий"],
                    [20, "Больной"],
                    [0, "Умирающий"]
                ],
                "satiety": [
                    [120, "Объевшийся"],
                    [75, "Сытый"],
                    [50, "Проголодавшийся"],
                    [25, "Голодный"],
                    [0, "Истощённый"]
                ],
                "mood": [
                    [120, "Счастливый"],
                    [75, "Весёлый"],
                    [50, "Довольный"],
                    [30, "Грустный"],
                    [20, "Раздражённый"],
                    [5, "Злой"],
                    [0, "Озлобленный"]
                ]
            },
            # шкалы состояний для запросов тамагочи (None - ничего не нужно)
            "scales_for_query": {
                "age": [
                    [21, None],
                    [0, "Я стар."]
                ],
                "health": [
                    [60, None],
                    [40, "Мне плохо. Дай лекарство."],
                    [20, "Я болен. Вылечи меня."],
                    [0, "Я умираю. Меня срочно нужно лечить."]
                ],
                "satiety": [
                    [120, "Я объелся. Не корми меня."],
                    [75, None],
                    [50, "Я проголодался. Угости меня."],
                    [25, "Я голоден. Покорми меня."],
                    [0, "Я истощён. Меня срочно надо покормить."]
                ],
                "mood": [
                    [50, None],
                    [30, "Мне грустно. Развесели меня."],
                    [20, "Я раздражён. Успокой меня."],
                    [5, "Я зол на тебя. Ты - плохой друг. Докажи, что это не так."],
                    [0, "Жизнь - кошмар. Я уйду."]
                ]
            },
            "properties": {
                "age": {"balance": 100, "decrease_step": -5, "interval": 60, "increase_step": 0,
                        "status": "Ребенок"},
                "health": {"balance": 100, "decrease_step": -8, "interval": 12, "increase_step": 10,
                           "status": "Здоровый"},
                "satiety": {"balance": 100, "decrease_step": -8, "interval": 6, "increase_step": 10, "status": "Сытый"},
                "mood": {"balance": 100, "decrease_step": -6, "interval": 3, "increase_step": 10, "status": "Весёлый"}
            }
        }
    }
